from django.db import models

# Create your models here.
class Profile(models.Model):
    name = models.CharField(max_length=100)
    phone = models.CharField(max_length=100)
    email = models.CharField(max_length=100)
    unniversite= models.CharField(max_length=100)
    niveau= models.TextField(max_length=1000)
    about =models.TextField(max_length=1000)
    programmation=models.TextField(max_length=1000)
